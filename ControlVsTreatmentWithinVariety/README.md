# What's here

This directory contains code, image, and data files related to differential expression analysis. 

Look in `results` directory for code output such as lists of differentially expressed genes. 
Also look in `results` for images saved from [Integrated Genome Browser](https://bioviz.org) or other graphical tools. 

Please note:

The code in this directory depends on:

* code from Common.R (in `src` directory one level up)
* data files from the **Count** module 
