# Welcome #

This repository contains data processing and analysis code for RNA-Seq analysis of gene expression in pollen tubes.

# How to use this repository

* Use git to clone this repository onto your computer
* Install R and RStudio on your computer 
* Use RStudio to open RStudio project files (extension `.Rproj`) from the cloned repository

Within each project, run .Rmd and .R files. Note that you may need to install libraries such as edgeR.

# About what's here

* Each subdirectory module, e.g., `Count`, contains code, input data files, and results files from one or more related analyses. 
* Files imported from external sources or pipelines are stored in directories named `data`. These files are used as inputs for analysis code stored here. 
* Files produced by code stored here as well as images produced by [Integrated Genome Browser](https://bioviz.org.) are stored in directories named `results`. 
* The top-level `src` directory contains shell and python scripts used to process or deploy RNA-Seq data. 

# Genome version used

All results files produced here come from the SL4.0 (S_lycopersicum_Sep_2019) Heinz tomato genome assembly and ITAG4.0 gene models. 

Genome data and annotations have been re-formatted for visual analysis in [Integrated Genome Browser](https://bioviz.org) and deployed on-line in the IGBQuickload site: [http://igbquickload.org/quickload/S_lycopersicum_Sep_2019](http://igbquickload.org/quickload/S_lycopersicum_Sep_2019).

### Project participants ###

* Rob Reid
* Mark Johnson
* James Pease
* Sorel Ouonkap Yimga
* Ann Loraine
* Ben Styler 
* Rachel Blanding
