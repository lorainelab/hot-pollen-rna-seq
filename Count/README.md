# What's here

This directory contains output from counting the number of aligned fragments per gene, per sample.

* data/counts.txt - output from featureCounts, fragments per gene
* data/codebook.txt - table with sample information

The files with prefix **ClusterCounts** report results from MDS clustering of the top, most highly expressed genes per sample. 

Major findings:

* Samples cluster by variety 
* Samples show weak association by replicate number
