#!/bin/bash
#PBS -q copperhead
#PBS -l nodes=1:ppn=8
#PBS -l mem=64gb
#PBS -l walltime=8:00:00
#PBS -N featureCounts
# see bitbucket.org/lorainelab/tomatopollen
SRC=$HOME/src/tomatopollen
cd $PBS_O_WORKDIR
module load subread
SAF=$SRC/GeneRegions/results/SAF.txt
BASE=counts
OUT=$BASE.txt
ERR=$BASE.err
OUT2=$BASE.out
# use primary alignment only
featureCounts -T 8 -O -C --primary -F SAF -a $SAF -o $OUT *.bam 2>$ERR 1>$OUT2
